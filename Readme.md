# Instruções
Segue as intruções de uso da aplicação.

## JAVA 10.0.2
Foi optado pelo JAVA 10, devido uma incompatibilidade do ASM usado na execução dos testes pelo gradle com o JAVA 11.
<br/>
Mais informações sobre o problema: https://github.com/gradle/gradle/issues/5731

## Swagger
Para que seja possível utilizar a API,foi disponibilizado o Swagger.
<br/>
Após a inicialização da API:
<br/>
http://localhost:8080/swagger-ui.html

## Console DB H2
Após inicializar a aplicação, chamar: http://localhost:8080/h2-console/

## Banco de Dados
Database: H2
<br/>
**AMBIENTE PRODUTIVO**: Ao inicializar a aplicação, será gerado um arquivo "financial.mv.db" que trata-se do banco de dados da aplicação.
<br/>
**TESTES**: Ao executar os testes, será gerado um arquivo "financial-test.mv.db".

## Implantação da API
1. Clonar a API<br/>
2. Entrar no diretório e executar (aqui os testes já são executados). Caso um teste não passe, o build da aplicação não irá ocorrer.
```text
gradlew build
```
3. Entrar no diretório "build/libs" e executar 
```text
java -jar financial-{version}-SNAPSHOT.jar
``` 
4. Chamar Swagger: http://localhost:8080/swagger-ui.html

## Cross-Domain
Não foi habilitado para isso, devido não haver a necessidade para o teste da empresa.
