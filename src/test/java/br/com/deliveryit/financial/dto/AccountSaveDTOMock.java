package br.com.deliveryit.financial.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

public class AccountSaveDTOMock {
    public static AccountSaveDTO insert_in_success() {
        AccountSaveDTO accountDTO = new AccountSaveDTO();
        accountDTO.setDueDate(LocalDate.parse("2019-02-01"));
        accountDTO.setName("Conta de Luz");
        accountDTO.setPaydate(LocalDate.parse("2019-02-04"));
        accountDTO.setOriginalValue(new BigDecimal(10.98));
        return accountDTO;
    }

    public static AccountSaveDTO insert_expected_success() {
        AccountSaveDTO accountDTO = new AccountSaveDTO();
        accountDTO.setId(1L);
        accountDTO.setDueDate(LocalDate.parse("2019-02-01"));
        accountDTO.setName("Conta de Luz");
        accountDTO.setPaydate(LocalDate.parse("2019-02-04"));
        accountDTO.setOriginalValue(new BigDecimal(10.98));
        return accountDTO;
    }

    public static AccountSaveDTO insertWithoutName_in_failed() {
        AccountSaveDTO accountDTO = new AccountSaveDTO();
        accountDTO.setDueDate(LocalDate.parse("2019-02-01"));
        accountDTO.setPaydate(LocalDate.parse("2019-02-04"));
        accountDTO.setOriginalValue(new BigDecimal(10.98));
        return accountDTO;
    }

    public static AccountSaveDTO insertToUpdate_in_success() {
        //8 dias de atraso
        AccountSaveDTO accountDTO = new AccountSaveDTO();
        accountDTO.setDueDate(LocalDate.parse("2019-02-02"));
        accountDTO.setName("Conta de Luz Update");
        accountDTO.setPaydate(LocalDate.parse("2019-02-10"));
        accountDTO.setOriginalValue(new BigDecimal(99.99));
        return accountDTO;
    }

    public static AccountSaveDTO insertToList_in_success() {
        //8 dias de atraso
        AccountSaveDTO accountDTO = new AccountSaveDTO();
        accountDTO.setDueDate(LocalDate.parse("2019-03-25"));
        accountDTO.setName("Conta de Luz Update - List");
        accountDTO.setPaydate(LocalDate.parse("2019-03-30"));
        accountDTO.setOriginalValue(new BigDecimal(100));
        return accountDTO;
    }
}
