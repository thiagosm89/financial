package br.com.deliveryit.financial.tests;

import br.com.deliveryit.financial.dto.AccountSaveDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

import static org.springframework.test.util.AssertionErrors.assertEquals;

@RunWith(BlockJUnit4ClassRunner.class)
public class AccountDTOTest {
    @Test
    public void whenNewAccountWithoutNameAttr_thenErrorWillThrown() {
        AccountSaveDTO accountDTO = new AccountSaveDTO();
        accountDTO.setDueDate(LocalDate.parse("2019-02-01"));
        accountDTO.setPaydate(LocalDate.parse("2019-02-04"));
        accountDTO.setOriginalValue(new BigDecimal(10.98));

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<AccountSaveDTO>> validate = validator.validate(accountDTO);

        ConstraintViolation<AccountSaveDTO> next = validate.iterator().next();
        assertEquals("Name not validated","{message.name.required}",
                next.getMessage());
    }

    @Test
    public void whenNewAccountWithoutDueDateAttr_thenErrorWillThrown() {
        AccountSaveDTO accountDTO = new AccountSaveDTO();
        accountDTO.setName("Conta de Luz");
        accountDTO.setPaydate(LocalDate.parse("2019-02-04"));
        accountDTO.setOriginalValue(new BigDecimal(10.98));

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<AccountSaveDTO>> validate = validator.validate(accountDTO);

        ConstraintViolation<AccountSaveDTO> next = validate.iterator().next();
        assertEquals("DueDate not validated","{message.duedate.required}",
                next.getMessage());
    }

    @Test
    public void whenNewAccountWithoutPaydateAttr_thenErrorWillThrown() {
        AccountSaveDTO accountDTO = new AccountSaveDTO();
        accountDTO.setName("Conta de Luz");
        accountDTO.setDueDate(LocalDate.parse("2019-02-04"));
        accountDTO.setOriginalValue(new BigDecimal(10.98));

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<AccountSaveDTO>> validate = validator.validate(accountDTO);

        ConstraintViolation<AccountSaveDTO> next = validate.iterator().next();
        assertEquals("Paydate not validated","{message.paydate.required}",
                next.getMessage());
    }

    @Test
    public void whenNewAccountWithoutOriginalValueAttr_thenErrorWillThrown() {
        AccountSaveDTO accountDTO = new AccountSaveDTO();
        accountDTO.setName("Conta de Luz");
        accountDTO.setDueDate(LocalDate.parse("2019-02-04"));
        accountDTO.setPaydate(LocalDate.parse("2019-02-04"));

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<AccountSaveDTO>> validate = validator.validate(accountDTO);

        ConstraintViolation<AccountSaveDTO> next = validate.iterator().next();
        assertEquals("OriginalValue not validated","{message.originalvalue.required}",
                next.getMessage());
    }
}