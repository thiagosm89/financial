package br.com.deliveryit.financial.tests;

import br.com.deliveryit.financial.dto.AccountDTO;
import br.com.deliveryit.financial.dto.AccountSaveDTO;
import br.com.deliveryit.financial.dto.AccountSaveDTOMock;
import br.com.deliveryit.financial.service.AccountService;
import br.com.deliveryit.financial.service.AccountServiceImpl;
import org.flywaydb.core.Flyway;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import javax.sql.DataSource;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class AccountServiceImplTest {
    @TestConfiguration
    static class AccountServiceImplTestContextConfiguration {
        @Autowired
        private DataSource dataSource;

        @Bean
        public Flyway flyway() {
            Flyway load = Flyway.configure()
                    .dataSource(dataSource)
                    .locations("classpath:db/migration")
                    .load();
            load.clean();
            load.migrate();
            return load;
        }

        @Bean
        public ModelMapper modelMapper() {
            return new ModelMapper();
        }

        @Bean
        public AccountService accountService() {
            return new AccountServiceImpl();
        }
    }

    @Autowired
    private AccountService accountService;

    @Autowired
    private ModelMapper modelMapper;

    @Test
    public void whenNewAccount_thenAccountShouldBeCreated() {
        Mono<AccountSaveDTO> save = accountService
                .save(AccountSaveDTOMock.insert_in_success())
                .doOnError(Throwable::printStackTrace);

        StepVerifier
                .create(save)
                .expectNextMatches(accountSaveDTO -> Optional.ofNullable(accountSaveDTO.getId()).isPresent())
                .verifyComplete();
    }

    @Test
    public void whenNewAccountWithoutNameRequired_thenErrorIsThrown() {
        Mono<AccountSaveDTO> save = accountService.save(AccountSaveDTOMock.insertWithoutName_in_failed());

        StepVerifier
                .create(save)
                .expectError()
                .verifyThenAssertThat();
    }

    @Test
    public void whenUpdateAccountNameCorrectly_thenAccountShouldBeUpdated() {
        Mono<AccountSaveDTO> update =
                accountService.save(AccountSaveDTOMock.insertToUpdate_in_success())
                .map(accountSaveDTO -> {
                    Assert.assertEquals("Conta de Luz Update", accountSaveDTO.getName());
                    accountSaveDTO.setName("Conta de Luz Updated");
                    Mono<AccountSaveDTO> save = accountService.save(accountSaveDTO);
                    return save.block();
                }).doOnError(Throwable::printStackTrace);

        StepVerifier
                .create(update)
                .expectNextMatches(accountSaveDTO -> accountSaveDTO.getName().equals("Conta de Luz Updated"))
                .verifyComplete();
    }

    @Test
    public void whenGetallAccounts_thenListAll() {
        Mono<AccountSaveDTO> save = accountService.save(AccountSaveDTOMock.insertToList_in_success());
        AccountSaveDTO dtoResult = save.block();
        Assert.assertNotNull(dtoResult);
        AccountDTO dtoExpect = modelMapper.map(dtoResult, AccountDTO.class);
        Flux<AccountDTO> accountDTOFlux = accountService.findAll()
                .doOnError(Throwable::printStackTrace);
        Mono<AccountDTO> next = accountDTOFlux.filter(a -> a.getId().equals(dtoExpect.getId()))
                .next();
        StepVerifier
                .create(next)
                .expectNextMatches(a -> a.getId().equals(dtoExpect.getId()))
                .verifyComplete();
    }
}
