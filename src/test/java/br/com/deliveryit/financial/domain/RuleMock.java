package br.com.deliveryit.financial.domain;

import java.math.BigDecimal;

public class RuleMock {
    public static Rule save_in_success() {
        Rule rule = new Rule();
        rule.setId(1L);
        rule.setDelayedDays(3);
        rule.setLatePaymentPenaltyPercentage(new BigDecimal(2.00));
        rule.setDailyInterestPercentage(new BigDecimal(0.10));
        rule.setOperation(Rule.Operation.LTE);
        return rule;
    }

    public static Rule ruleGT5() {
        Rule rule = new Rule();
        rule.setId(3L);
        rule.setDelayedDays(5);
        rule.setLatePaymentPenaltyPercentage(new BigDecimal(5));
        rule.setDailyInterestPercentage(new BigDecimal(0.3));
        rule.setOperation(Rule.Operation.GT);
        return rule;
    }
}
