INSERT INTO PUBLIC.RULE (id, operation, delayed_days, late_payment_penalty_percentage, daily_interest_percentage)
VALUES (1, 'LTE', 3, 2.0, 0.1);

INSERT INTO PUBLIC.RULE (id, operation, delayed_days, late_payment_penalty_percentage, daily_interest_percentage)
VALUES (2, 'GT', 3, 3.0, 0.2);

INSERT INTO PUBLIC.RULE (id, operation, delayed_days, late_payment_penalty_percentage, daily_interest_percentage)
VALUES (3, 'GT', 5, 5.0, 0.3);