CREATE TABLE PUBLIC.ACCOUNT
(
  id INTEGER NOT NULL AUTO_INCREMENT,
  name VARCHAR(200) NOT NULL,
  original_value DECIMAL(15, 2) NOT NULL,
  corrected_value DECIMAL(15, 2) NOT NULL,
  number_days_overdue INTEGER NOT NULL,
  due_date DATE NOT NULL,
  paydate DATE NOT NULL,
  rule_id INTEGER NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE PUBLIC.RULE
(
  id INTEGER NOT NULL AUTO_INCREMENT,
  operation VARCHAR(3),
  delayed_days SMALLINT,
  late_payment_penalty_percentage DECIMAL(3, 2) NOT NULL,
  daily_interest_percentage DECIMAL(3, 2) NOT NULL,
  PRIMARY KEY (id)
);

ALTER TABLE PUBLIC.ACCOUNT
ADD FOREIGN KEY (RULE_ID) REFERENCES PUBLIC.RULE(ID);

ALTER TABLE PUBLIC.RULE
ADD CONSTRAINT RULE_UNIQUE
UNIQUE ( DELAYED_DAYS, OPERATION );