package br.com.deliveryit.financial.controller;

import br.com.deliveryit.financial.dto.RuleDTO;
import br.com.deliveryit.financial.service.RuleService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/rule")
public class RuleController {

    @Autowired
    private RuleService ruleService;

    @GetMapping("/getall")
    @ApiOperation(value = "${apidoc.rule.getall.value}",
            response = RuleDTO.class,
            responseContainer = "List"
    )
    private Flux<RuleDTO> getAll() {
        return ruleService.findAll();
    }
}
