package br.com.deliveryit.financial.controller;

import br.com.deliveryit.financial.dto.AccountDTO;
import br.com.deliveryit.financial.dto.AccountSaveDTO;
import br.com.deliveryit.financial.service.AccountService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @GetMapping("/getall")
    @ApiOperation(value = "${apidoc.account.getall.value}",
            notes = "${apidoc.account.getall.notes}",
            response = AccountDTO.class,
            responseContainer = "List"
    )
    private Flux<AccountDTO> getAll() {
        return accountService.findAll();
    }

    @GetMapping("/getpaging")
    @ApiOperation(value = "${apidoc.account.getall.paging.value}",
            notes = "${apidoc.account.getall.paging.notes}",
            response = AccountDTO.class,
            responseContainer = "List"
    )
    private Flux<AccountDTO> getAll(@RequestParam("page") final int page,
                                    @RequestParam("size") final int size) {
        return accountService.findAll(PageRequest.of(page, size));
    }

    @PostMapping
    @ApiOperation(value = "${apidoc.account.save.value}",
            response = AccountSaveDTO.class)
    private Mono<AccountSaveDTO> save(@RequestBody @Valid AccountSaveDTO accountDTO) {
        return accountService.save(accountDTO);
    }
}
