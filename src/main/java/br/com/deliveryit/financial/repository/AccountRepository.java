package br.com.deliveryit.financial.repository;

import br.com.deliveryit.financial.domain.Account;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AccountRepository extends PagingAndSortingRepository<Account, Long> {
}