package br.com.deliveryit.financial.repository.custom;

import br.com.deliveryit.financial.domain.Rule;

import java.util.List;

public interface RuleRepositoryCustom {
    List<Rule> findAllToRules();
}