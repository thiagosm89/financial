package br.com.deliveryit.financial.repository;

import br.com.deliveryit.financial.domain.Rule;
import br.com.deliveryit.financial.repository.custom.RuleRepositoryCustom;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RuleRepository extends PagingAndSortingRepository<Rule, Long>, RuleRepositoryCustom {
}