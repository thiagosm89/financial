package br.com.deliveryit.financial.repository.custom;

import br.com.deliveryit.financial.domain.Rule;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
@Transactional(readOnly = true)
public class RuleRepositoryImpl implements RuleRepositoryCustom {
    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List<Rule> findAllToRules() {
        String hql = "FROM Rule as r ORDER BY r.delayedDays ASC, " +
                "CASE " +
                "   WHEN r.operation = :lt THEN 1" +
                "   WHEN r.operation = :lte THEN 2" +
                "   WHEN r.operation = :gte THEN 3" +
                "   WHEN r.operation = :gt THEN 4 " +
                "END";

        Query query = entityManager.createQuery(hql);
        query.setParameter("lt", Rule.Operation.LT);
        query.setParameter("lte", Rule.Operation.LTE);
        query.setParameter("gte", Rule.Operation.GTE);
        query.setParameter("gt", Rule.Operation.GT);
        return query.getResultList();
    }
}
