package br.com.deliveryit.financial.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class RuleException extends ResponseStatusException {
    public RuleException(String reason) {
        super(HttpStatus.BAD_REQUEST, reason);
    }

    public RuleException(String reason, Throwable cause) {
        super(HttpStatus.BAD_REQUEST, reason, cause);
    }
}
