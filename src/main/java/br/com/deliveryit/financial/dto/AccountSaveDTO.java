package br.com.deliveryit.financial.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

public class AccountSaveDTO extends EntityDTO implements Serializable {

    private static final long serialVersionUID = -7048235656911148803L;

    @NotBlank(message = "{message.name.required}")
    private String name;
    @NotNull(message = "{message.originalvalue.required}")
    private BigDecimal originalValue;
    @NotNull(message = "{message.duedate.required}")
    private LocalDate dueDate;
    @NotNull(message = "{message.paydate.required}")
    private LocalDate paydate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getOriginalValue() {
        return originalValue;
    }

    public void setOriginalValue(BigDecimal originalValue) {
        this.originalValue = originalValue;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public LocalDate getPaydate() {
        return paydate;
    }

    public void setPaydate(LocalDate paydate) {
        this.paydate = paydate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountSaveDTO that = (AccountSaveDTO) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(originalValue, that.originalValue) &&
                Objects.equals(dueDate, that.dueDate) &&
                Objects.equals(paydate, that.paydate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, originalValue, dueDate, paydate);
    }

    @Override
    public String toString() {
        return "AccountSaveDTO{" +
                "name='" + name + '\'' +
                ", originalValue=" + originalValue +
                ", dueDate=" + dueDate +
                ", paydate=" + paydate +
                '}';
    }
}
