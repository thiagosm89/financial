package br.com.deliveryit.financial.dto;

import br.com.deliveryit.financial.domain.Rule;

import java.io.Serializable;
import java.math.BigDecimal;

public class RuleDTO extends EntityDTO implements Serializable {
    private Integer delayedDays;
    private BigDecimal latePaymentPenaltyPercentage;
    private BigDecimal dailyInterestPercentage;
    private Rule.Operation operation;

    public Integer getDelayedDays() {
        return delayedDays;
    }

    public void setDelayedDays(Integer delayedDays) {
        this.delayedDays = delayedDays;
    }

    public BigDecimal getLatePaymentPenaltyPercentage() {
        return latePaymentPenaltyPercentage;
    }

    public void setLatePaymentPenaltyPercentage(BigDecimal latePaymentPenaltyPercentage) {
        this.latePaymentPenaltyPercentage = latePaymentPenaltyPercentage;
    }

    public BigDecimal getDailyInterestPercentage() {
        return dailyInterestPercentage;
    }

    public void setDailyInterestPercentage(BigDecimal dailyInterestPercentage) {
        this.dailyInterestPercentage = dailyInterestPercentage;
    }

    public Rule.Operation getOperation() {
        return operation;
    }

    public void setOperation(Rule.Operation operation) {
        this.operation = operation;
    }
}
