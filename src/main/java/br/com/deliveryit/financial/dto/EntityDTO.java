package br.com.deliveryit.financial.dto;

public class EntityDTO {
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
