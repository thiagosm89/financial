package br.com.deliveryit.financial.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

public class AccountDTO extends EntityDTO implements Serializable {

    private static final long serialVersionUID = 8298854671042716311L;
    private String name;
    private BigDecimal originalValue;
    private BigDecimal correctedValue;
    private Integer numberDaysOverdue;
    private LocalDate paydate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getOriginalValue() {
        return originalValue;
    }

    public void setOriginalValue(BigDecimal originalValue) {
        this.originalValue = originalValue;
    }

    public LocalDate getPaydate() {
        return paydate;
    }

    public void setPaydate(LocalDate paydate) {
        this.paydate = paydate;
    }

    public BigDecimal getCorrectedValue() {
        return correctedValue;
    }

    public void setCorrectedValue(BigDecimal correctedValue) {
        this.correctedValue = correctedValue;
    }

    public Integer getNumberDaysOverdue() {
        return numberDaysOverdue;
    }

    public void setNumberDaysOverdue(Integer numberDaysOverdue) {
        this.numberDaysOverdue = numberDaysOverdue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountDTO that = (AccountDTO) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(originalValue, that.originalValue) &&
                Objects.equals(correctedValue, that.correctedValue) &&
                Objects.equals(numberDaysOverdue, that.numberDaysOverdue) &&
                Objects.equals(paydate, that.paydate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, originalValue, correctedValue, numberDaysOverdue, paydate);
    }

    @Override
    public String toString() {
        return "AccountDTO{" +
                "name='" + name + '\'' +
                ", originalValue=" + originalValue +
                ", correctedValue=" + correctedValue +
                ", numberDaysOverdue=" + numberDaysOverdue +
                ", paydate=" + paydate +
                '}';
    }
}
