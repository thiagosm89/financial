package br.com.deliveryit.financial.domain;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "ACCOUNT")
public class Account extends GenericEntity {

    @Column(name = "NAME", nullable = false)
    private String name;
    @Column(name = "ORIGINAL_VALUE", nullable = false)
    private BigDecimal originalValue;
    @Column(name = "DUE_DATE", nullable = false)
    private LocalDate dueDate;
    @Column(name = "PAYDATE", nullable = false)
    private LocalDate paydate;
    @Column(name = "NUMBER_DAYS_OVERDUE", nullable = false)
    private int numberDaysOverdue;
    private BigDecimal correctedValue;
    @ManyToOne
    @JoinColumn(name = "rule_id", referencedColumnName = "id")
    private Rule rule;

    @Column(name = "id", updatable = false, nullable = false)
    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getOriginalValue() {
        return originalValue;
    }

    public void setOriginalValue(BigDecimal originalValue) {
        this.originalValue = originalValue;
    }

    public BigDecimal getCorrectedValue() {
        return correctedValue;
    }

    public void setCorrectedValue(BigDecimal correctedValue) {
        this.correctedValue = correctedValue;
    }

    public int getNumberDaysOverdue() {
        return numberDaysOverdue;
    }

    public void setNumberDaysOverdue(int numberDaysOverdue) {
        this.numberDaysOverdue = numberDaysOverdue;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public LocalDate getPaydate() {
        return paydate;
    }

    public void setPaydate(LocalDate paydate) {
        this.paydate = paydate;
    }

    public Rule getRule() {
        return rule;
    }

    public void setRule(Rule rule) {
        this.rule = rule;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return numberDaysOverdue == account.numberDaysOverdue &&
                Objects.equals(name, account.name) &&
                Objects.equals(originalValue, account.originalValue) &&
                Objects.equals(dueDate, account.dueDate) &&
                Objects.equals(paydate, account.paydate) &&
                Objects.equals(rule, account.rule);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, originalValue, dueDate, paydate, numberDaysOverdue, rule);
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", originalValue=" + originalValue +
                ", dueDate=" + dueDate +
                ", paydate=" + paydate +
                ", numberDaysOverdue=" + numberDaysOverdue +
                '}';
    }
}