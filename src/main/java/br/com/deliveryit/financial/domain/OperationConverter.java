package br.com.deliveryit.financial.domain;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class OperationConverter implements AttributeConverter<Rule.Operation, String> {

    @Override
    public String convertToDatabaseColumn(Rule.Operation attribute) {
        return attribute.toString();
    }

    @Override
    public Rule.Operation convertToEntityAttribute(String dbData) {
        for (Rule.Operation operation : Rule.Operation.values()) {
            if( operation.toString().equals(dbData) )
                return operation;
        }
        return null;
    }
}