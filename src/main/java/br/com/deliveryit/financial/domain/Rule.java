package br.com.deliveryit.financial.domain;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "RULE")
public class Rule extends GenericEntity {
    @Column(name = "DELAYED_DAYS")
    private Integer delayedDays;
    @Column(name = "LATE_PAYMENT_PENALTY_PERCENTAGE")
    private BigDecimal latePaymentPenaltyPercentage;
    @Column(name = "DAILY_INTEREST_PERCENTAGE")
    private BigDecimal dailyInterestPercentage;
    @Column(name = "OPERATION")
    private Operation operation;

    @Override
    @Column(name = "id", updatable = false, nullable = false)
    public Long getId() {
        return id;
    }

    public Integer getDelayedDays() {
        return delayedDays;
    }

    public void setDelayedDays(Integer delayedDays) {
        this.delayedDays = delayedDays;
    }

    public BigDecimal getLatePaymentPenaltyPercentage() {
        return latePaymentPenaltyPercentage;
    }

    public void setLatePaymentPenaltyPercentage(BigDecimal latePaymentPenaltyPercentage) {
        this.latePaymentPenaltyPercentage = latePaymentPenaltyPercentage;
    }

    public BigDecimal getDailyInterestPercentage() {
        return dailyInterestPercentage;
    }

    public void setDailyInterestPercentage(BigDecimal dailyInterestPercentage) {
        this.dailyInterestPercentage = dailyInterestPercentage;
    }

    @Convert(converter = OperationConverter.class)
    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public enum Operation {
        GTE, GT, LTE, LT;
    }

    public boolean isLTE() {
        return getOperation().equals(Operation.LTE);
    }

    public boolean isLT() {
        return getOperation().equals(Operation.LT);
    }

    public boolean isGTE() {
        return getOperation().equals(Operation.GTE);
    }

    public boolean isGT() {
        return getOperation().equals(Operation.GT);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rule rule = (Rule) o;
        return Objects.equals(delayedDays, rule.delayedDays) &&
                Objects.equals(latePaymentPenaltyPercentage, rule.latePaymentPenaltyPercentage) &&
                Objects.equals(dailyInterestPercentage, rule.dailyInterestPercentage) &&
                operation == rule.operation;
    }

    @Override
    public int hashCode() {
        return Objects.hash(delayedDays, latePaymentPenaltyPercentage, dailyInterestPercentage, operation);
    }

    @Override
    public String toString() {
        return "Rule{" +
                "delayedDays=" + delayedDays +
                ", latePaymentPenaltyPercentage=" + latePaymentPenaltyPercentage +
                ", dailyInterestPercentage=" + dailyInterestPercentage +
                ", operation=" + operation +
                ", id=" + id +
                '}';
    }
}
