package br.com.deliveryit.financial.service;

import br.com.deliveryit.financial.domain.Account;
import br.com.deliveryit.financial.domain.Rule;
import br.com.deliveryit.financial.dto.AccountDTO;
import br.com.deliveryit.financial.dto.AccountSaveDTO;
import br.com.deliveryit.financial.exception.RuleException;
import br.com.deliveryit.financial.repository.AccountRepository;
import br.com.deliveryit.financial.repository.RuleRepository;
import org.joda.time.Days;
import org.joda.time.Instant;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountRepository repository;
    @Autowired
    private RuleRepository ruleRepository;
    @Autowired
    private ModelMapper mapper;

    @Override
    public Flux<AccountDTO> findAll() {
        return Flux.fromIterable(repository.findAll())
                .map(account -> mapper.map(account, AccountDTO.class));
    }

    @Override
    public Flux<AccountDTO> findAll(Pageable pageable) {
        return Flux.fromIterable(repository.findAll(pageable))
                .map(account -> mapper.map(account, AccountDTO.class));
    }

    @Override
    public Mono<AccountSaveDTO> save(AccountSaveDTO accountDTO) {
        Mono<Account> map = Mono.just(accountDTO)
                .map(dto -> mapper.map(dto, Account.class))
                .map(this::ajustNumberDaysOverdue)
                .map(this::defineRule)
                .map(this::correctedValue);

        Mono<Account> insert = map.filter(account -> !Optional.ofNullable(account.getId()).isPresent());

        Mono<Account> update = map.filter(account -> Optional.ofNullable(account.getId()).isPresent())
                .map(account -> {
                    Optional<Account> maybeAccountDB = repository.findById(account.getId());
                    if (maybeAccountDB.isPresent()) {
                        Account accountUpdated = maybeAccountDB.get();
                        mapper.map(accountDTO, accountUpdated);
                        return accountUpdated;
                    } else {
                        return account;
                    }
                });

        Mono<Account> accountFlux = insert.concatWith(update).last();
        return accountFlux
                .map(account -> repository.save(account))
                .map(account -> mapper.map(account, AccountSaveDTO.class));
    }

    private Account ajustNumberDaysOverdue(Account account) {
        if (account.getPaydate().isAfter(account.getDueDate())) {
            Instant paydate = Instant.parse(account.getPaydate().toString());
            Instant dueDate = Instant.parse(account.getDueDate().toString());
            Days days = Days.daysBetween(dueDate, paydate);
            account.setNumberDaysOverdue(days.getDays());
        }
        return account;
    }

    private Account defineRule(Account account) {
        int numberDaysOverdue = account.getNumberDaysOverdue();

        Flux<Rule> rules = Flux.fromIterable(ruleRepository.findAllToRules());

        Mono<Rule> reduce = rules.reduce((rule, accumulator) -> {
            switch (rule.getOperation()) {
                case LT: //Rule is Less Than
                    if (rule.getDelayedDays() < accumulator.getDelayedDays() &&
                            numberDaysOverdue < rule.getDelayedDays()) {
                        return rule;
                    } else if (rule.getDelayedDays().equals(accumulator.getDelayedDays())) {
                        if (accumulator.isLTE() || accumulator.isLT()) {
                            throw new RuleException("Conflicting Rules. RULE 01: " + rule.toString()
                                    + " confliting with RULE 02: " + rule.toString());
                        } else if (numberDaysOverdue < rule.getDelayedDays()) {
                            return rule;
                        }
                    }
                    return accumulator;
                case LTE: //Rule is Less Than or Equal
                    if (rule.getDelayedDays() < accumulator.getDelayedDays() &&
                            numberDaysOverdue <= rule.getDelayedDays()) {
                        return rule;
                    } else if (rule.getDelayedDays().equals(accumulator.getDelayedDays())) {
                        if (accumulator.isLTE() || accumulator.isLT() || accumulator.isGTE()) {
                            throw new RuleException("Conflicting Rules. RULE 01: " + rule.toString()
                                    + " confliting with RULE 02: " + rule.toString());
                        } else if (numberDaysOverdue <= rule.getDelayedDays()) {
                            return rule;
                        }
                    }
                    return accumulator;
                case GTE: //Rule is Greater Than or Equal
                    if (rule.getDelayedDays() < accumulator.getDelayedDays() &&
                            numberDaysOverdue >= accumulator.getDelayedDays()) {
                        return accumulator;
                    } else if (rule.getDelayedDays().equals(accumulator.getDelayedDays())) {
                        if (accumulator.isLTE() || accumulator.isGT() || accumulator.isGTE()) {
                            throw new RuleException("Conflicting Rules. RULE 01: " + rule.toString()
                                    + " confliting with RULE 02: " + rule.toString());
                        } else if (numberDaysOverdue >= accumulator.getDelayedDays()) {
                            return accumulator;
                        }
                    }

                    if (accumulator.isLT() || accumulator.isLTE()) {
                        throw new RuleException("Conflicting Rules. RULE 01: " + rule.toString()
                                + " confliting with RULE 02: " + accumulator.toString());
                    }
                    return rule;
                case GT: //Rule is Greater Than
                    if (rule.getDelayedDays() < accumulator.getDelayedDays() &&
                            numberDaysOverdue > accumulator.getDelayedDays()) {
                        return accumulator;
                    } else if (rule.getDelayedDays().equals(accumulator.getDelayedDays())) {
                        if (accumulator.isGT() || accumulator.isGTE()) {
                            throw new RuleException("Conflicting Rules. RULE 01: " + rule.toString()
                                    + " confliting with RULE 02: " + accumulator.toString());
                        } else if (numberDaysOverdue > accumulator.getDelayedDays()) {
                            return accumulator;
                        }
                    }

                    if (accumulator.isLT() || accumulator.isLTE()) {
                        throw new RuleException("Conflicting Rules. RULE 01: " + rule.toString()
                                + " confliting with RULE 02: " + accumulator.toString());
                    }
                    return rule;
            }
            return rule;
        });
        Rule rule = reduce.block();
        if( rule == null) {
            throw new RuleException("Not found rules in database.");
        }

        if ( rule.isLTE() && !(numberDaysOverdue <= rule.getDelayedDays()) ||
                rule.isLT() && !(numberDaysOverdue < rule.getDelayedDays()) ||
                rule.isGT() && !(numberDaysOverdue > rule.getDelayedDays()) ||
                rule.isGTE() && !(numberDaysOverdue >= rule.getDelayedDays())
        ) {
            throw new RuleException("No valid rule found.");
        }
        account.setRule(rule);
        return account;
    }

    private Account correctedValue(Account account) {
        BigDecimal originalValue = account.getOriginalValue();
        Rule rule = account.getRule();
        BigDecimal penalty = originalValue.multiply(
                rule.getLatePaymentPenaltyPercentage().divide(
                        new BigDecimal(100), 4, RoundingMode.HALF_EVEN
                )
        ).setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal dailyInterest = originalValue.multiply(
                rule.getDailyInterestPercentage().divide(new BigDecimal(100), 4, RoundingMode.HALF_EVEN)
                        .multiply(new BigDecimal(account.getNumberDaysOverdue()))
                        .setScale(2, RoundingMode.HALF_EVEN)
        ).setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal correctedValue = originalValue.add(penalty).add(dailyInterest);
        account.setCorrectedValue(correctedValue.setScale(2, RoundingMode.HALF_EVEN));
        return account;
    }
}
