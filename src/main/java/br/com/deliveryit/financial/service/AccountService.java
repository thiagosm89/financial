package br.com.deliveryit.financial.service;

import br.com.deliveryit.financial.dto.AccountDTO;
import br.com.deliveryit.financial.dto.AccountSaveDTO;
import org.springframework.data.domain.Pageable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface AccountService {
    Flux<AccountDTO> findAll();

    Flux<AccountDTO> findAll(Pageable pageable);

    Mono<AccountSaveDTO> save(AccountSaveDTO accountDTO );
}
