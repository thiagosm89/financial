package br.com.deliveryit.financial.service;

import br.com.deliveryit.financial.dto.RuleDTO;
import br.com.deliveryit.financial.repository.RuleRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
public class RuleServiceImpl implements RuleService {
    @Autowired
    private RuleRepository ruleRepository;
    @Autowired
    private ModelMapper mapper;

    @Override
    public Flux<RuleDTO> findAll() {
        return Flux.fromIterable(ruleRepository.findAll())
                .map(rule -> mapper.map(rule, RuleDTO.class));
    }
}
