package br.com.deliveryit.financial.service;

import br.com.deliveryit.financial.dto.RuleDTO;
import reactor.core.publisher.Flux;

public interface RuleService {
    Flux<RuleDTO> findAll();
}
